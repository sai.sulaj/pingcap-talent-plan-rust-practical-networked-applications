use redis_ping_pong_3::deserializer::RespDeserializer;
use std::io::{self, Write};
use ascii::AsciiChar;

fn serialize_command(command: &str) -> Vec<u8> {
    let mut serialized_bytes: Vec<u8> = vec!['*' as u8];
    let terms: Vec<&str> = command
        .trim()
        .split_whitespace()
        .collect();
    
    let mut num_terms: Vec<u8> = terms.len()
        .to_string()
        .as_bytes()
        .to_vec();

    serialized_bytes.append(&mut num_terms);
    serialized_bytes.push(AsciiChar::CarriageReturn as u8);
    serialized_bytes.push(AsciiChar::LineFeed as u8);

    for term in terms {
        let mut term_bytes: Vec<u8> = term.bytes().collect();
        serialized_bytes.push('$' as u8);
        let mut str_len = term_bytes.len()
            .to_string()
            .as_bytes()
            .to_vec();
        serialized_bytes.append(&mut str_len);
        serialized_bytes.push(AsciiChar::CarriageReturn as u8);
        serialized_bytes.push(AsciiChar::LineFeed as u8);
        serialized_bytes.append(&mut term_bytes);
        serialized_bytes.push(AsciiChar::CarriageReturn as u8);
        serialized_bytes.push(AsciiChar::LineFeed as u8);
    }

    serialized_bytes
}

fn main() {
    let stdin = io::stdin();
    let mut input_buf = String::new();
    let mut buffer: Vec<u8> = vec![0; 1024];

    loop {
        print!("> ");
        io::stdout().flush().expect("Error writing to STDOUT");
        input_buf.clear();
        stdin.read_line(&mut input_buf).expect("Error reading from STDIN");
        let command_bytes = serialize_command(&input_buf);
        let mut command_bufread = io::BufReader::new(&command_bytes[..]);
        let resp_des = RespDeserializer::new(&mut command_bufread).read_terms(&mut buffer);
        println!("RESP: {:?}", resp_des);
    }
}
