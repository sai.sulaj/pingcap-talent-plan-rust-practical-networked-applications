use snafu::Snafu;

#[derive(Debug, Snafu)]
#[snafu(visibility = "pub")]
pub enum Error {
    #[snafu(display("Unable to deserialize stream: {}", source))]
    Deserializing {
        source: std::io::Error,
    },
    #[snafu(display("No data in stream"))]
    NoData {},
    #[snafu(display("Unknown RESP term type: {}", byte))]
    UnknownTermType { byte: char },
    #[snafu(display("Unable to deserialze invalid UTF-8 sequence: {}", source))]
    InvalidUtf8 { source: std::str::Utf8Error, },
    #[snafu(display("Unable to deserialze invalid integer: {}", source))]
    InvalidInteger { source: std::num::ParseIntError, },
}

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug)]
pub enum NullType {
    BulkStr,
    Array,
}

#[derive(Debug)]
pub enum RespTerm {
    SimpleStr(String),
    Error(String),
    Integer(i64),
    BulkStr(String),
    Null(NullType),
    Array(Vec<RespTerm>),
}
