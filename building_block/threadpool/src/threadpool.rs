use crate::types::{self, Result};
use snafu::ResultExt;
use std::{
    sync::{self, Condvar, Arc, Mutex},
    thread,
    collections::VecDeque,
    time::Duration,
};

enum PoolStatus {
    Running,
    Terminated,
}

type Job = Box<dyn FnOnce() + Send + 'static>;
type PoolState = Arc<(
    Mutex<VecDeque<Job>>,
    Mutex<PoolStatus>,
    Condvar
)>;

macro_rules! clone_all {
    ($($i:ident),+) => {
        $(let $i = $i.clone();)+
    }
}

pub struct ThreadPool {
    pool_state: PoolState,
    threads: Vec<thread::JoinHandle<()>>,
}

fn thread_loop(pool_state: &PoolState) -> Result<PoolStatus> {
    let (queue_lock, status_lock, cvar) = &**pool_state;
    let mut queue: sync::MutexGuard<VecDeque<Job>> = match queue_lock.lock() {
        Ok(q) => q,
        Err(_) => return types::QueueLockError {}.fail(),
    };

    while queue.len() == 0 {
        match status_lock.try_lock() {
            Ok(v) => {
                if let PoolStatus::Terminated = *v {
                    return Ok(PoolStatus::Terminated);
                }
            },
            Err(_) => (),
        }
        queue = match cvar.wait_timeout(queue, Duration::from_millis(20)) {
            Ok(q) => q.0,
            Err(_) => return types::QueueLockError {}.fail(),
        };
    }

    let job: Job = match queue.pop_front() {
        Some(j) => j,
        None => return Ok(PoolStatus::Running),
    };
    drop(queue);

    println!("Job received on thread {:?}", thread::current().name());
    job();

    Ok(PoolStatus::Running)
}

impl ThreadPool {
    pub fn new(num_threads: u32) -> Result<Self> {
        let mut threads: Vec<thread::JoinHandle<()>> = Vec::new();

        let pool_state = Arc::new((
            Mutex::new(VecDeque::new()),
            Mutex::new(PoolStatus::Running),
            Condvar::new(),
        ));

        for thread_num in 0..num_threads {
            let next_thread = thread::Builder::new()
                .name(format!("{}", thread_num))
                .spawn({clone_all!(pool_state); move || {
                    println!("Thread {:?} initialized", thread::current().name());

                    loop {
                        match thread_loop(&pool_state) {
                            Ok(status) => {
                                if let PoolStatus::Terminated = status {
                                    break;
                                }
                            },
                            Err(err) => println!("{}", err),
                        };
                    }

                    println!("Thread {:?} terminated", thread::current().name());
                }}).context(types::InitThreadError)?;
            threads.push(next_thread);
        }

        Ok(ThreadPool {
            pool_state,
            threads,
        })
    }

    pub fn terminate(&mut self) -> Result<()> {
        let (_, pool_status, cvar) = &*self.pool_state;
        let mut status: sync::MutexGuard<PoolStatus> = match pool_status.lock() {
            Ok(s) => s,
            Err(_) => return types::StatusLockError {}.fail(),
        };
        *status = PoolStatus::Terminated;
        cvar.notify_all();

        drop(status);

        while self.threads.len() > 0 {
            let thread = self.threads.pop().unwrap();
            thread.join().unwrap_or_else(|_| {
                println!("{}", types::JoinThreadsError {}.build());
            });
        }

        Ok(())
    }

    pub fn spawn<F>(&mut self, job: F) -> Result<()> where
        F: FnOnce() + Send + 'static {
        let (queue_lock, _, cvar) = &*self.pool_state;
        let mut queue = match queue_lock.lock() {
            Ok(q) => q,
            Err(_) => return types::QueueLockError {}.fail(),
        };

        queue.push_back(Box::new(job));

        drop(queue);
        cvar.notify_one();

        Ok(())
    }
}

impl Drop for ThreadPool {
    fn drop (&mut self) {
        self.terminate().unwrap();
    }
}
