use threadpool::{ThreadPool, Result};
use std::{
    time::Duration,
    thread,
};

fn job_1() {
    println!("Job 1 executing");
    thread::sleep(Duration::from_millis(2000));
    println!("Job 1 executed");
}

fn main() -> Result<()> {
    let mut pool = ThreadPool::new(2)?;

    for _ in 0..10 {
        pool.spawn(job_1)?;
    }

    Ok(())
}
