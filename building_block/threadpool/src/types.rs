use snafu::Snafu;
use std::io;

#[derive(Debug, Snafu)]
#[snafu(visibility = "pub")]
pub enum Error {
    #[snafu(display("Unable to init thread: {}", source))]
    InitThreadError { source: io::Error },
    #[snafu(display("Unable to access job: {}", source))]
    AccessJobError { source: io::Error },
    #[snafu(display("Unable to lock queue"))]
    QueueLockError {},
    #[snafu(display("Unable to lock status"))]
    StatusLockError {},
    #[snafu(display("Unable to join threads"))]
    JoinThreadsError {},
}

pub type Result<A, E = Error> = std::result::Result<A, E>;
