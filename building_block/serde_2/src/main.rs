use serde::{
    Serialize,
    Deserialize,
};
use serde_json;
use bson;
use std::{
    fs::{File},
    io::{self, BufRead, prelude::*},
    str,
};

#[derive(Serialize, Deserialize, Debug)]
struct Move {
    delta_x: i32,
    delta_y: i32,
}
impl Move {
    pub fn new(delta_x: i32, delta_y: i32) -> Self {
        Move { delta_x, delta_y }
    }
}

fn exercise_1() -> io::Result<()> {
    println!("Exercise 1:\n");
    let w_file = File::create("exercise_1.txt")?;
    let a: Move = Move::new(1, 2);

    serde_json::to_writer(w_file, &a)?;

    let r_file = File::open("exercise_1.txt")?;
    let b: Move = serde_json::from_reader(io::BufReader::new(r_file))?;

    println!("a: {:?}, b: {:?}", a, b);

    Ok(())
}

fn exercise_2() -> io::Result<()> {
    println!("Exercise 2:\n");
    let a = Move::new(2, 3);
    let serialized: Vec::<u8> = serde_json::to_vec(&a)?;
    println!("a serialized: {:?}", serialized);

    let serialized_str = str::from_utf8(&serialized).unwrap();

    println!("a serialized str: {:?}", serialized_str);

    Ok(())
}

fn exercise_3() -> io::Result<()> {
    println!("Exercise 3:\n");
    let mut w_file = File::create("exercise_3.txt")?;

    for i in 0..1000 {
        let a = Move::new(i, i + 1);
        let serialized = bson::to_bson(&a).unwrap();

        if let bson::Bson::Document(doc) = serialized {
            bson::encode_document(&mut w_file, doc.iter()).unwrap();
            w_file.write_all(b"|")?;
        }
    }

    let r_file = File::open("exercise_3.txt")?;
    let mut reader = io::BufReader::new(r_file);
    let mut byte_vec: Vec::<u8> = Vec::new();

    let mut count = 0;
    loop {
        let bytes = reader.read_until(b'|', &mut byte_vec)?;
        match bson::decode_document(&mut &byte_vec[..]) {
            Ok(d) => d,
            Err(_) => break,
        };
        count += 1;
        byte_vec.clear();
        if bytes == 0 { break; }
    }

    println!("Total read: {}", count);
    Ok(())
}

fn main() {
    exercise_1().unwrap();
    exercise_2().unwrap();
    exercise_3().unwrap();
}
