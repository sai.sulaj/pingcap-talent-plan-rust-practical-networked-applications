use crate::{kv_store::constants::SIZE_PREFIX_BYTES, Result, error};
use snafu::ResultExt;
use std::convert::TryInto;
use std::{
    fs,
    io::{BufReader, BufWriter, Read, Write},
};

pub fn write_sized_data(writable: &mut BufWriter<&mut fs::File>, data: &[u8]) -> Result<()> {
    let data_len = data.len();
    let mut pos = 0;
    while pos < data_len {
        pos += writable.write(&data[pos..]).context(error::WritingStream)?;
    }

    Ok(())
}

pub fn read_sized_data(readable: &mut BufReader<&mut fs::File>) -> Result<Option<Vec<u8>>> {
    let mut handle = readable.take(SIZE_PREFIX_BYTES as u64);
    let mut buffer = vec![0u8; 1024];
    let mut bytes_read = 0usize;

    while bytes_read < SIZE_PREFIX_BYTES {
        bytes_read += handle.read(&mut buffer[bytes_read..]).context(error::ReadingStream)?;
        if bytes_read == 0 {
            return Ok(None);
        }
    }

    // Buffer length with allways be >= 4.
    let data_len = i32::from_le_bytes(buffer[0..SIZE_PREFIX_BYTES].try_into().unwrap());

    handle.set_limit(data_len as u64 - SIZE_PREFIX_BYTES as u64);
    while bytes_read < data_len as usize {
        bytes_read += handle.read(&mut buffer[bytes_read..]).context(error::ReadingStream)?;
    }

    buffer.truncate(bytes_read);
    Ok(Some(buffer))
}

fn set_buffer_length(vec: &mut Vec<u8>, len: usize) {
    let diff = len - vec.len();

    for _ in 0..diff {
        vec.push(0);
    }
}

pub fn scan_sized_stream<'a>(
    readable: &mut BufReader<&mut fs::File>,
    mut callback: Box<dyn FnMut(&[u8], usize) -> Result<Option<Vec<u8>>> + 'a>,
) -> Result<Option<Vec<u8>>> {
    let mut buffer: Vec<u8> = vec![0; 10];
    let mut handle = readable.take(0);

    let mut pos = 0;

    loop {
        let mut bytes_read = 0;
        handle.set_limit(SIZE_PREFIX_BYTES as u64);

        while bytes_read < SIZE_PREFIX_BYTES {
            bytes_read += handle.read(&mut buffer[bytes_read..]).context(error::ReadingStream)?;
            if bytes_read == 0 {
                return Ok(None);
            }
        }

        // Buffer length with allways be >= 4.
        let data_len = i32::from_le_bytes(buffer[0..SIZE_PREFIX_BYTES].try_into().unwrap());

        if buffer.len() < data_len as usize {
            set_buffer_length(&mut buffer, data_len as usize);
        }

        handle.set_limit(data_len as u64 - SIZE_PREFIX_BYTES as u64);

        let mut attempts = 0;
        while bytes_read < data_len as usize {
            let bytes_diff = handle.read(&mut buffer[bytes_read..]).context(error::ReadingStream)?;
            if bytes_diff == 0 {
                attempts += 1;
            }
            if attempts >= 3 {
                return Ok(None);
            }
            bytes_read += bytes_diff;
        }

        pos += bytes_read;
        let ret_val = callback(&buffer[0..bytes_read], pos)?;

        if ret_val.is_some() {
            return Ok(ret_val);
        }
    }
}
