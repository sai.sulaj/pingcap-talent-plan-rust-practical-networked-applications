use chrono::{DateTime, Utc};
use dotenv_codegen::dotenv;
use snafu::ResultExt;
use std::{
    collections::HashMap,
    fs,
    io::{BufReader, BufWriter, Seek, SeekFrom},
    path,
};

use crate::{
    if_verbose,
    kv_store::{
        command::{Command, RemoveCommand, SetCommand},
        constants,
        index_file::IndexFile,
        log_pointer::LogPointer,
        size_prefixed_byte_stream_io,
    },
    Result,
    error,
};

fn compare_log_dates(left: &str, right: &str) -> Result<bool> {
    let left = match left.parse::<DateTime<Utc>>() {
        Ok(l) => l,
        Err(_) => return error::Generic { message: "Unable to parse logfile date.".to_string() }.fail(),
    };
    let right = match right.parse::<DateTime<Utc>>() {
        Ok(l) => l,
        Err(_) => return error::Generic { message: "Unable to parse logfile date.".to_string() }.fail(),
    };

    Ok(left > right)
}

pub struct KvStore {
    dir_path: String,
}
impl KvStore {
    pub fn set(&mut self, key: String, value: String) -> Result<()> {
        if_verbose!(println!("KvStore.set -> key: {}, value: {}", key, value));
        let dir_path = path::Path::new(&self.dir_path);
        let latest_log_path = KvStore::get_active_log_path(dir_path)?;
        let mut log_file = KvStore::open_logfile(&latest_log_path)?;
        let command = Command::Set(SetCommand::new(key.clone(), value));
        let pos = command.write_to_log(&mut log_file)?;
        let log_pointer = LogPointer::new(key, latest_log_path, pos as i64);
        let index_file = IndexFile::new(self.dir_path.clone())?;
        index_file.set_log_pointer(log_pointer)?;

        if KvStore::should_compact(dir_path)? {
            self.compact()?;
        }

        Ok(())
    }

    fn open_logfile(path: &str) -> Result<fs::File> {
        if_verbose!(println!("KvStore::open_logfile -> path: {}", path));
        let path = path::Path::new(&path);
        let file = fs::OpenOptions::new()
            .read(true)
            .create(true)
            .append(true)
            .open(path)
            .context(error::ReadingFile)?;

        Ok(file)
    }

    fn get_command(&mut self, key: String) -> Result<Option<Command>> {
        if_verbose!(println!("KvStore.get_command -> key: {}", key));
        let index_file = IndexFile::new(self.dir_path.to_string())?;
        let log_pointer = match index_file.get_log_pointer(&key)? {
            Some(lp) => lp,
            None => return Ok(None),
        };

        let mut log_file = KvStore::open_logfile(&log_pointer.file_path)?;
        let mut reader = BufReader::new(&mut log_file);

        reader.seek(SeekFrom::Start(log_pointer.pos as u64)).context(error::ReadingFile)?;

        let result = size_prefixed_byte_stream_io::read_sized_data(&mut reader)?;
        match result {
            Some(mut buffer) => {
                let command = Command::deserialize(&mut buffer)?;
                Ok(Some(command))
            }
            None => Ok(None),
        }
    }

    pub fn get(&mut self, key: String) -> Result<Option<String>> {
        if_verbose!(println!("KvStore.get -> key: {}", key));
        let command: Command = match self.get_command(key.clone())? {
            Some(comm) => comm,
            None => return Ok(None),
        };

        match command {
            Command::Set(set_command) => Ok(Some(set_command.value)),
            _ => Ok(None),
        }
    }

    pub fn remove(&mut self, key: String) -> Result<()> {
        if_verbose!(println!("KvStore.remove -> key: {}", key));
        let dir_path = path::Path::new(&self.dir_path);
        let latest_log_path = KvStore::get_active_log_path(dir_path)?;
        let mut log_file = KvStore::open_logfile(&latest_log_path)?;
        let index_file = IndexFile::new(self.dir_path.clone())?;

        if index_file.get_log_pointer(&key)?.is_none() {
            return error::KeyNotFound {}.fail();
        }

        let command = Command::Remove(RemoveCommand::new(key.clone()));
        command.write_to_log(&mut log_file)?;
        index_file.remove_log_pointer(&key)?;
        Ok(())
    }

    fn get_active_log_path(dir_path: &path::Path) -> Result<String> {
        if_verbose!(println!(
            "KvStore::get_active_log_path -> path: {:?}",
            dir_path
        ));
        let log_bytes_max: u64 = dotenv!("LOG_BYTES_MAX").to_string().parse::<u64>()
            .expect("LOG_BYTES_MAX env variable not found");

        let latest_log_path: Option<String> = fs::read_dir(&dir_path).context(error::ReadingFile)?.fold(None, |acc, entry| {
            let entry: fs::DirEntry = entry.ok()?;
            let metadata = entry.metadata().ok()?;
            if !metadata.is_file() || metadata.len() >= log_bytes_max {
                return acc;
            }

            let entry_name = entry.file_name().into_string().ok()?;
            if !entry_name.ends_with(constants::LOG_EXTENSION) {
                return acc;
            }

            match acc {
                Some(acc_name) => {
                    if compare_log_dates(&entry_name, &acc_name).ok()? {
                        return Some(entry_name);
                    }

                    Some(acc_name)
                }
                None => Some(entry_name),
            }
        });

        let active_log_path: path::PathBuf = match latest_log_path {
            Some(target_filename) => dir_path.join(target_filename),
            None => dir_path.join(format!(
                "{}{}",
                Utc::now().to_rfc3339_opts(chrono::SecondsFormat::Millis, false),
                constants::LOG_EXTENSION
            )),
        };

        Ok(active_log_path.to_str().unwrap().to_string())
    }

    fn compact(&self) -> Result<()> {
        if_verbose!(println!("KvStore.compact"));

        let mut log_file_paths: Vec<path::PathBuf> = fs::read_dir(&self.dir_path).context(error::ReadingFile)?
            .filter_map(|entry| {
                let entry: fs::DirEntry = entry.ok()?;
                let entry_path = entry.path();
                if entry_path.as_path().ends_with(constants::INDEX_FILE_NAME) {
                    return None;
                }
                let file_name = entry_path.as_path().file_name();
                if file_name.is_none() {
                    return None;
                }
                let file_name = file_name.unwrap().to_str();
                if file_name.is_none() {
                    return None;
                }
                let file_name = file_name.unwrap();
                if !file_name.ends_with(constants::LOG_EXTENSION) {
                    return None;
                }
                let metadata = entry.metadata().ok()?;
                if metadata.is_file() {
                    Some(entry.path())
                } else {
                    None
                }
            })
            .collect();

        // Sort from oldest to newest.
        log_file_paths.sort_by(|a, b| {
            a.file_name()
                .unwrap()
                .partial_cmp(b.file_name().unwrap())
                .unwrap()
        });

        let mut command_map: HashMap<String, String> = HashMap::new();

        for log_path in log_file_paths.iter() {
            let mut file = fs::File::open(log_path).context(error::ReadingFile)?;
            let mut reader = BufReader::new(&mut file);

            let read_callback = |bytes: &[u8], _| -> Result<Option<Vec<u8>>> {
                let command = Command::deserialize(bytes)?;
                match command {
                    Command::Set(set_command) => {
                        command_map.insert(set_command.key.clone(), set_command.value);
                    }
                    Command::Remove(remove_command) => {
                        command_map.remove(&remove_command.key);
                    }
                }

                Ok(None)
            };

            size_prefixed_byte_stream_io::scan_sized_stream(&mut reader, Box::new(read_callback))?;
        }

        let next_log_path = path::Path::new(&self.dir_path).join(format!(
            "{}{}",
            Utc::now().to_rfc3339_opts(chrono::SecondsFormat::Millis, false),
            constants::LOG_EXTENSION
        ));
        let mut next_log_file = KvStore::open_logfile(next_log_path.to_str().unwrap())?;
        let mut writable = BufWriter::new(&mut next_log_file);

        for log_file_path in log_file_paths.iter() {
            fs::remove_file(log_file_path).context(error::ManipulatingFile)?;
        }

        let index_file_path = path::Path::new(&self.dir_path).join(constants::INDEX_FILE_NAME);
        let mut index_file = fs::OpenOptions::new()
            .read(true)
            .create(true)
            .write(true)
            .open(index_file_path)
            .context(error::ReadingFile)?;
        let mut index_file_writable = BufWriter::new(&mut index_file);

        let mut pos = 0;
        for (_, entry) in command_map.iter().enumerate() {
            let command = Command::Set(SetCommand::new(entry.0.to_owned(), entry.1.to_owned()));
            let command_bson = command.to_bson()?;
            let mut buffer: Vec<u8> = Vec::new();
            bson::encode_document(&mut buffer, &command_bson).context(error::SerializeBson)?;
            size_prefixed_byte_stream_io::write_sized_data(&mut writable, &mut buffer)?;
            let log_pointer = LogPointer::new(
                entry.0.to_owned(),
                next_log_path.to_str().unwrap().to_string(),
                pos,
            );
            let log_pointer_bson = log_pointer.to_bson()?;
            let mut buffer = Vec::new();
            bson::encode_document(&mut buffer, &log_pointer_bson).context(error::SerializeBson)?;
            size_prefixed_byte_stream_io::write_sized_data(&mut index_file_writable, &mut buffer)?;
            pos += buffer.len() as i64;
        }

        Ok(())
    }

    fn should_compact(path: &path::Path) -> Result<bool> {
        let num_bytes: u64 = fs::read_dir(&path).context(error::ReadingFile)?
            .filter_map(|entry| {
                let entry = entry;
                if entry.is_err() {
                    return None;
                }
                let entry = entry.as_ref().unwrap();
                let metadata = entry.metadata();
                if metadata.is_err() {
                    return None;
                }
                let metadata = metadata.unwrap();
                if !metadata.is_file() {
                    return None;
                }
                let filename = entry.file_name();
                let fn_attemp = filename.to_str();

                if let Some(name) = fn_attemp {
                    if !name.ends_with(constants::LOG_EXTENSION) {
                        return None;
                    }
                    return Some(metadata.len());
                }

                None
            })
            .sum();

        Ok(num_bytes > dotenv!("MAX_LOGFILE_BYTES").parse::<u64>().expect("MAX_LOGFILE_BYTES env variable not found"))
    }

    pub fn open(path: &path::Path) -> Result<Self> {
        if_verbose!(println!("KvStore::open -> path: {:?}", path));
        let path = path.canonicalize().context(error::ManipulatingFile)?;

        if !path.exists() {
            fs::create_dir(&path).context(error::ManipulatingFile)?;
        } else if !path.is_dir() {
            return error::NonExistentPath {}.fail();
        }

        let kv_store = KvStore {
            dir_path: path.to_str().unwrap().to_string(),
        };

        if KvStore::should_compact(path.as_path())? {
            kv_store.compact()?;
        }

        Ok(kv_store)
    }
}
