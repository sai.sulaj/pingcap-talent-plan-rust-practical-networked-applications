use bson::{self, Bson, Document};
use dotenv_codegen::dotenv;
use snafu::ResultExt;
use serde::{Deserialize, Serialize};
use std::{
    fs,
    io::{BufWriter, Seek, SeekFrom},
};

use crate::{if_verbose, kv_store::size_prefixed_byte_stream_io, Result, error};

#[derive(Serialize, Deserialize, Debug)]
pub struct SetCommand {
    pub key: String,
    pub value: String,
}
impl SetCommand {
    pub fn new(key: String, value: String) -> Self {
        if_verbose!(println!(
            "SetCommand::new -> key: {}, value: {}",
            key, value
        ));
        SetCommand { key, value }
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct RemoveCommand {
    pub key: String,
}
impl RemoveCommand {
    pub fn new(key: String) -> Self {
        if_verbose!(println!("RemoveCommand::new -> key: {}", key));
        RemoveCommand { key }
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub enum Command {
    Set(SetCommand),
    Remove(RemoveCommand),
}
impl Command {
    pub fn to_bson(&self) -> Result<Document> {
        if_verbose!(println!("Command.to_bson"));
        match bson::to_bson(&self).context(error::SerializeBson)? {
            Bson::Document(doc) => Ok(doc),
            _ => error::Serialize {
                message: "Unable to serialize command to bson document".to_string()
            }.fail(),
        }
    }

    pub fn write_to_log(&self, file: &mut fs::File) -> Result<u64> {
        if_verbose!(println!("Command.write_to_log -> file: {:?}", file));
        file.seek(SeekFrom::End(0)).context(error::ReadingFile)?;
        let pos = file.metadata().context(error::ReadingFile)?.len();
        let mut writer = BufWriter::new(file);

        let doc: Document = self.to_bson()?;
        let mut serialized: Vec<u8> = Vec::new();

        bson::encode_document(&mut serialized, &doc)
            .context(error::SerializeBson)?;
        size_prefixed_byte_stream_io::write_sized_data(&mut writer, &serialized)?;
        Ok(pos)
    }

    pub fn deserialize(bytes: &[u8]) -> Result<Self> {
        if_verbose!(println!("Command::deserialize -> bytes: {:?}", bytes));
        let doc: Document =
            bson::decode_document(&mut &bytes[..]).context(error::DeserializeBson)?;
        let command: Command = bson::from_bson(Bson::Document(doc))
            .context(error::DeserializeBson)?;

        Ok(command)
    }
}
