use bson;
use dotenv_codegen::dotenv;
use snafu::ResultExt;
use std::{
    collections::HashMap,
    fs,
    io::{BufReader, BufWriter, Seek, SeekFrom},
    path,
};

use crate::{
    if_verbose,
    kv_store::{
        command::Command, constants, log_pointer::LogPointer, size_prefixed_byte_stream_io,
    },
    error,
    Result,
};

pub struct IndexFile {
    dir_path: String,
}
impl IndexFile {
    pub fn new(dir_path: String) -> Result<Self> {
        if_verbose!(println!("IndexFile::new -> dir_path: {:?}", dir_path));
        Ok(IndexFile { dir_path })
    }

    fn open_index_file(&self) -> Result<fs::File> {
        if_verbose!(println!("IndexFile::open_index_file"));
        let file_path = path::Path::new(&self.dir_path).join(constants::INDEX_FILE_NAME);
        let file = fs::OpenOptions::new()
            .read(true)
            .create(true)
            .write(true)
            .open(file_path)
            .context(error::ReadingFile)?;

        Ok(file)
    }

    pub fn get_log_pointer(&self, key: &str) -> Result<Option<LogPointer>> {
        if_verbose!(println!("IndexFile.get_log_pointer -> key: {:?}", key));
        let mut file = self.open_index_file()?;
        let mut reader = BufReader::new(&mut file);

        let callback = move |bytes: &[u8], _: usize| -> Result<Option<Vec<u8>>> {
            let log_pointer = LogPointer::deserialize(bytes)?;

            if log_pointer.key == key {
                return Ok(Some(bytes.to_vec()));
            }

            Ok(None)
        };

        let result =
            size_prefixed_byte_stream_io::scan_sized_stream(&mut reader, Box::new(callback))?;

        if result.is_some() {
            let target = LogPointer::deserialize(&mut result.unwrap())?;
            return Ok(Some(target));
        }

        Ok(None)
    }

    pub fn set_log_pointer(&self, log_pointer: LogPointer) -> Result<()> {
        if_verbose!(println!(
            "IndexFile.set_log_pointer -> log_pointer: {:?}",
            log_pointer
        ));
        let mut file = self.open_index_file()?;
        let mut reader = BufReader::new(&mut file);
        let log_pointer_doc = log_pointer.to_bson()?;
        let mut log_pointer_serialized = Vec::new();
        bson::encode_document(&mut log_pointer_serialized, &log_pointer_doc).context(error::SerializeBson)?;

        let tempfile_path = path::Path::new(&self.dir_path).join("tmp.kvs");

        let mut tempfile = fs::OpenOptions::new()
            .create(true)
            .write(true)
            .truncate(true)
            .open(&tempfile_path)
            .context(error::ReadingFile)?;
        let mut writer = BufWriter::new(&mut tempfile);

        let mut replaced = false;
        let replaced_ref = &mut replaced;
        let callback = move |bytes: &[u8], _: usize| -> Result<Option<Vec<u8>>> {
            let candidate = LogPointer::deserialize(bytes)?;
            if candidate.key == log_pointer.key {
                *replaced_ref = true;
                size_prefixed_byte_stream_io::write_sized_data(
                    &mut writer,
                    &log_pointer_serialized,
                )?;
            } else {
                size_prefixed_byte_stream_io::write_sized_data(&mut writer, &bytes)?;
            }

            Ok(None)
        };

        size_prefixed_byte_stream_io::scan_sized_stream(&mut reader, Box::new(callback))?;

        if !replaced {
            // #lifetimesbewyldin
            let mut writer = BufWriter::new(&mut tempfile);
            writer.seek(SeekFrom::End(0)).context(error::ReadingFile)?;
            let mut log_pointer_serialized = Vec::new();
            bson::encode_document(&mut log_pointer_serialized, &log_pointer_doc).context(error::SerializeBson)?;
            size_prefixed_byte_stream_io::write_sized_data(&mut writer, &log_pointer_serialized)?;
        }

        fs::rename(
            &tempfile_path,
            path::Path::new(&self.dir_path).join(constants::INDEX_FILE_NAME),
        ).context(error::ManipulatingFile)?;

        Ok(())
    }

    pub fn remove_log_pointer<'a>(&self, key: &'a str) -> Result<()> {
        if_verbose!(println!("IndexFile.remove_log_pointer -> key: {:?}", key));
        let mut file = self.open_index_file()?;
        let mut reader = BufReader::new(&mut file);
        let tempfile_path = path::Path::new(&self.dir_path).join("tmp.kvs");

        let mut tempfile = fs::OpenOptions::new()
            .create(true)
            .write(true)
            .truncate(true)
            .open(&tempfile_path)
            .context(error::ReadingFile)?;
        let mut writer = BufWriter::new(&mut tempfile);

        let callback = move |bytes: &[u8], _: usize| -> Result<Option<Vec<u8>>> {
            let candidate = LogPointer::deserialize(bytes)?;
            if candidate.key != key {
                size_prefixed_byte_stream_io::write_sized_data(&mut writer, &bytes)?;
            }

            Ok(None)
        };

        size_prefixed_byte_stream_io::scan_sized_stream(&mut reader, Box::new(callback))?;

        fs::rename(
            &tempfile_path,
            path::Path::new(&self.dir_path).join(constants::INDEX_FILE_NAME),
        ).context(error::ManipulatingFile)?;

        Ok(())
    }

    pub fn generate_index(&self) -> Result<()> {
        if_verbose!(println!("IndexFile.generate_index"));
        let dir_path = path::Path::new(&self.dir_path);
        if !dir_path.exists() {
            fs::create_dir(dir_path).context(error::ManipulatingFile)?;
        } else if !dir_path.is_dir() {
            return error::NonExistentPath {}.fail();
        }

        let mut log_pointer_file_paths: Vec<path::PathBuf> = fs::read_dir(dir_path)
            .context(error::ReadingFile)?
            .filter_map(|entry| {
                let entry: fs::DirEntry = entry.ok()?;
                let entry_path = entry.path();
                if entry_path.as_path().ends_with(constants::INDEX_FILE_NAME) {
                    return None;
                }
                let file_name = entry_path.as_path().file_name();
                if file_name.is_none() {
                    return None;
                }
                let file_name = file_name.unwrap().to_str();
                if file_name.is_none() {
                    return None;
                }
                let file_name = file_name.unwrap();
                if !file_name.ends_with(constants::LOG_EXTENSION) {
                    return None;
                }
                let metadata = entry.metadata().ok()?;
                if metadata.is_file() {
                    Some(entry.path())
                } else {
                    None
                }
            })
            .collect();

        // Sort from oldest to newest.
        log_pointer_file_paths.sort_by(|a, b| {
            a.file_name()
                .unwrap()
                .partial_cmp(b.file_name().unwrap())
                .unwrap()
        });

        let mut log_pointer_map: HashMap<String, LogPointer> = HashMap::new();

        for log_path in log_pointer_file_paths.iter() {
            let log_path_str: String = match log_path.to_str() {
                Some(log_path_str_ref) => log_path_str_ref.to_string(),
                None => continue,
            };
            let mut file = fs::File::open(log_path).context(error::ReadingFile)?;
            let mut reader = BufReader::new(&mut file);

            let callback = |bytes: &[u8], pos| -> Result<Option<Vec<u8>>> {
                let command = Command::deserialize(bytes)?;
                match command {
                    Command::Set(set_command) => {
                        let log_pointer = LogPointer::new(
                            set_command.key.clone(),
                            log_path_str.clone(),
                            pos as i64,
                        );
                        log_pointer_map.insert(set_command.key.clone(), log_pointer);
                    }
                    Command::Remove(remove_command) => {
                        log_pointer_map.remove(&remove_command.key);
                    }
                }

                Ok(None)
            };

            size_prefixed_byte_stream_io::scan_sized_stream(&mut reader, Box::new(callback))?;
        }

        let mut index_file = fs::OpenOptions::new()
            .write(true)
            .create(true)
            .truncate(true)
            .open(path::Path::new(&self.dir_path).join(constants::INDEX_FILE_NAME))
            .context(error::ReadingFile)?;
        let mut writable = BufWriter::new(&mut index_file);

        let mut buffer: Vec<u8> = Vec::new();
        for log_pointer in log_pointer_map.iter() {
            let doc: bson::Document = log_pointer.1.to_bson()?;
            bson::encode_document(&mut buffer, &doc)
                .context(error::SerializeBson)?;
            size_prefixed_byte_stream_io::write_sized_data(&mut writable, &buffer)?;
        }

        Ok(())
    }
}
