use bson::{self, Bson, Document};
use dotenv_codegen::dotenv;
use serde::{Deserialize, Serialize};
use std::str;
use snafu::ResultExt;

use crate::{if_verbose, Result, error};

#[derive(Serialize, Deserialize, Debug)]
pub struct LogPointer {
    pub key: String,
    pub file_path: String,
    pub pos: i64,
}
impl LogPointer {
    pub fn new(key: String, file_path: String, pos: i64) -> Self {
        if_verbose!(println!(
            "LogPointer::new -> key: {}, file_path: {}, pos: {}",
            key, file_path, pos
        ));
        LogPointer {
            key,
            file_path,
            pos,
        }
    }

    pub fn to_bson(&self) -> Result<Document> {
        if_verbose!(println!("LogPointer.to_bson"));
        match bson::to_bson(&self).context(error::SerializeBson)? {
            Bson::Document(doc) => Ok(doc),
            _ => error::Serialize {
                message: "Unable to serialize LogPointer to bson document".to_string()
            }.fail(),
        }
    }

    pub fn deserialize(bytes: &[u8]) -> Result<Self> {
        if_verbose!(println!("LogPointer::deserialize -> bytes: {:?}", bytes));
        let doc: Document =
            bson::decode_document(&mut &bytes[..]).context(error::DeserializeBson)?;
        let log_offset: LogPointer = bson::from_bson(Bson::Document(doc.clone()))
            .context(error::DeserializeBson)?;

        Ok(log_offset)
    }
}
