pub const LOG_EXTENSION: &str = ".kvs.log";
pub const INDEX_FILE_NAME: &str = ".index.kvs";

pub const SIZE_PREFIX_BYTES: usize = 4;
