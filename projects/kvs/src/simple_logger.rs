use log::{self, Record, Level, Metadata};

pub struct SimpleLogger;
impl log::Log for SimpleLogger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= log::max_level()
    }
    fn log(&self, record: &Record) {
        if !self.enabled(record.metadata()) { return; }
        match record.level() {
            Level::Error => eprintln!("{} - {}", record.level(), record.args()),
            _ => println!("{} - {}", record.level(), record.args()),
        }


    }
    fn flush(&self) {}
}
