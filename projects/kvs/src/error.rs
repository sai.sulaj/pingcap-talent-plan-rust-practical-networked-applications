use snafu::Snafu;
use bson;

#[derive(Debug, Snafu)]
#[snafu(visibility = "pub")]
pub enum Error {
    #[snafu(display("Unable to deserialize bson document: {}", source))]
    DeserializeBson { source: bson::DecoderError },
    #[snafu(display("Unable to deserialize stream: {}", message))]
    Deserialize { message: String },
    #[snafu(display("Unable to serialize bson document: {}", source))]
    SerializeBson { source: bson::EncoderError },
    #[snafu(display("Unable to serialize data: {}", message))]
    Serialize { message: String },
    #[snafu(display("Unable to read from stream: {}", source))]
    ReadingStream { source: std::io::Error },
    #[snafu(display("Unable to write to stream: {}", source))]
    WritingStream { source: std::io::Error },
    #[snafu(display("Unable to read file: {}", source))]
    ReadingFile { source: std::io::Error },
    #[snafu(display("Unable to manipulate file: {}", source))]
    ManipulatingFile { source: std::io::Error },
    #[snafu(display("Connecting to server: {}", source))]
    FailedServerConn { source: std::io::Error },
    #[snafu(display("Path does not exist"))]
    NonExistentPath {},
    #[snafu(display("Key not found"))]
    KeyNotFound {},
    #[snafu(display("{}", message))]
    Generic { message: String },
    #[snafu(display("Unable to deserialize redis stream: {}", source))]
    DeserializeRedis { source: std::io::Error },
    #[snafu(display("No data in stream"))]
    NoData {},
    #[snafu(display("Unknown RESP term type: {}", byte))]
    UnknownRespTermType { byte: char },
    #[snafu(display("Unable to deserialze invalid UTF-8 sequence: {}", source))]
    InvalidUtf8 { source: std::str::Utf8Error },
    #[snafu(display("Unable to deserialze invalid integer: {}", source))]
    InvalidInteger { source: std::num::ParseIntError },
    #[snafu(display("Invalid request: {}", message))]
    InvalidRequest { message: String },
}
