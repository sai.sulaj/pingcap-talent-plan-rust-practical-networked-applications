pub mod simple_logger;
pub mod kv_store;
pub mod kvs_engine;
pub use kv_store::KvStore;
pub use kvs_engine::KvsEngine;
pub mod redis;
pub mod error;
pub use threadpool as thread_pool;
use error::Error;

#[macro_export]
macro_rules! if_verbose {
    ($expr:expr) => {
        if (dotenv!("VERBOSE").to_lowercase() == "true") {
            $expr;
        }
    };
}

pub type Result<A> = std::result::Result<A, Error>;
