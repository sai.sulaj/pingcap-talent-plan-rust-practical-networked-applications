use std::{
    io::prelude::*,
    str,
};
use log::trace;
use snafu::ResultExt;

use crate::{
    redis::{
        constants,
        types,
    },
    error,
    Result,
};

fn allocate_buf(buf: &mut Vec<u8>, target_len: usize) {
    trace!("deserializer.rs:allocate_buf -> target_len: {}", target_len);
    if buf.len() >= target_len { return; }
    let diff = target_len - buf.len();
    for _ in 0..diff {
        buf.push(0);
    }
}

fn read_handle_bytes<'a>(
    reader: &mut std::io::Take<&mut (dyn std::io::BufRead + 'a)>,
    buffer: &mut [u8],
) -> Result<()> {
    trace!("deserializer.rs:read_handle_bytes -> LIMIT: {}", reader.limit());
    let mut bytes_read: u64 = 0;
    let mut failed_read_attempts: usize = 0;
    let target_bytes = reader.limit();

    while bytes_read < target_bytes {
        let bytes_delta = reader.read(buffer).context(error::DeserializeRedis)? as u64;
        if bytes_delta == 0 { failed_read_attempts += 1; }
        if failed_read_attempts >= constants::MAX_READ_ATTEMPTS {
            return error::NoData {}.fail();
        }

        bytes_read += bytes_delta;
    }

    Ok(())
}

pub struct RespDeserializer<'a> {
    reader: &'a mut (dyn BufRead + 'a),
}
impl <'a>RespDeserializer<'a> {
    pub fn new(reader: &'a mut (dyn BufRead + 'a)) -> Self {
        trace!("deserializer.rs:RespDeserializer::new");
        RespDeserializer { reader }
    }

    fn read_size_prefix(&mut self, buffer: &mut Vec<u8>) -> Result<i64> {
        trace!("deserializer.rs:RespDeserializer::read_size_prefix");
        let data_size: String = self.read_to_termination(buffer)?;
        let data_size: i64 = data_size.parse::<i64>().context(error::InvalidInteger)?;

        Ok(data_size)
    }

    fn read_to_termination(&mut self, buffer: &mut Vec<u8>) -> Result<String> {
        trace!("deserializer.rs:RespDeserializer::read_to_termination");
        let mut handle = self.reader.take(u64::MAX);
        buffer.clear();
        let bytes = handle.read_until('\r' as u8, buffer).context(error::DeserializeRedis)?;
        let string = str::from_utf8(&buffer[0..bytes - 1]).context(error::InvalidUtf8)?.to_string();

        // Consume newline after carriage return.
        handle.set_limit(1);
        read_handle_bytes(&mut handle, buffer)?;

        Ok(string)
    }

    fn read_array(&mut self, buffer: &mut Vec<u8>) -> Result<types::RespTerm> {
        trace!("deserializer.rs:RespDeserializer::read_array");
        let data_size = self.read_size_prefix(buffer)?;
        if data_size < 0 {
            return Ok(types::RespTerm::Null(types::NullType::Array));
        }

        let mut terms: Vec<types::RespTerm> = Vec::new();
        for _ in 0..data_size {
            let next_term = self.read_term(buffer)?;
            terms.push(next_term);
        }

        Ok(types::RespTerm::Array(terms))
    }

    fn read_bulk_str(&mut self, buffer: &mut Vec<u8>) -> Result<types::RespTerm> {
        trace!("deserializer.rs:RespDeserializer::read_bulk_str");
        let data_size = self.read_size_prefix(buffer)?;
        if data_size < 0 {
            return Ok(types::RespTerm::Null(types::NullType::BulkStr));
        }

        let mut handle = self.reader.take(data_size as u64);
        allocate_buf(buffer, data_size as usize);
        read_handle_bytes(&mut handle, buffer)?;

        let string = str::from_utf8(&buffer[0..data_size as usize])
            .context(error::InvalidUtf8)?.to_string();

        // Consume newline and carriage return.
        handle.set_limit(2);
        read_handle_bytes(&mut handle, buffer)?;

        Ok(types::RespTerm::BulkStr(string))
    }

    pub fn read_term(&mut self, buffer: &mut Vec<u8>) -> Result<types::RespTerm> {
        trace!("deserializer.rs:RespDeserializer::read_term");
        let mut handle = self.reader.take(1);
        read_handle_bytes(&mut handle, buffer)?;

        match buffer[0] as char {
            '+' => Ok(types::RespTerm::SimpleStr(self.read_to_termination(buffer)?)),
            '-' => Ok(types::RespTerm::Error(self.read_to_termination(buffer)?)),
            ':' => {
                let read_str = self.read_to_termination(buffer)?;
                let parsed_int: i64 = read_str.parse::<i64>().context(error::InvalidInteger)?;
                Ok(types::RespTerm::Integer(parsed_int))
            },
            '$' => self.read_bulk_str(buffer),
            '*' => self.read_array(buffer),
            _ => error::UnknownRespTermType { byte: buffer[0] }.fail(),
        }
    }

    pub fn read_terms(&mut self, buffer: &mut Vec<u8>) -> Result<Vec<types::RespTerm>> {
        trace!("deserializer.rs:RespDeserializer::read_terms");
        let mut terms: Vec<types::RespTerm> = Vec::new();

        loop {
            match self.read_term(buffer) {
                Ok(term) => terms.push(term),
                Err(err) => {
                    match err {
                        error::Error::NoData{} => break,
                        _ => return Err(err),
                    };
                },
            }
        }

        Ok(terms)
    }
}
