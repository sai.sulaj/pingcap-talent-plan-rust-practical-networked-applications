#[derive(Debug, Clone)]
pub enum NullType {
    BulkStr,
    Array,
}

#[derive(Debug, Clone)]
pub enum RespTerm {
    SimpleStr(String),
    Error(String),
    Integer(i64),
    BulkStr(String),
    Null(NullType),
    Array(Vec<RespTerm>),
}
