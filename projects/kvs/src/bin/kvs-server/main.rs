use clap::{load_yaml, App};
use dotenv_codegen::dotenv;
use log::{self, SetLoggerError, LevelFilter};
use snafu::ResultExt;
use std::{
    io::{self, prelude::*},
    net::{SocketAddr, TcpListener, TcpStream},
    env,
    path,
};

use kvs::{simple_logger::SimpleLogger, Result, error, redis, kv_store::KvStore};

static LOGGER: SimpleLogger = SimpleLogger;

fn init_logger() -> std::result::Result<(), SetLoggerError> {
    log::set_logger(&LOGGER)
        .map(|()| log::set_max_level(LevelFilter::Debug))
}

fn handle_error(err: error::Error) {
    println!("{}", err);
}

fn try_req_to_string_array(req: redis::types::RespTerm) -> Result<Vec<String>> {
    if let redis::types::RespTerm::Array(arr) = req {
        arr.iter().map(|term| {
            if let redis::types::RespTerm::BulkStr(s) = term {
                Ok(s.to_string())
            } else {
                error::Generic {
                    message: "Array elements not of type RespTerm::BulkStr",
                }.fail()
            }
        }).collect()
    } else {
        return error::Generic {
            message: "Term is not of type RespTerm::Array",
        }.fail();
    }
}

fn handle_get(kv_store: &mut KvStore, command: Vec<String>) -> Result<Option<String>> {
    if command.len() != 2 {
        return error::InvalidRequest {
            message: "GET command format: GET <key>".to_string(),
        }.fail();
    }

    // TODO: This sucks, need to implement general-purpose
    // redis serializer.
    match kv_store.get(command[1].clone())? {
        Some(value) => Ok(Some(format!("${}\r\n{}\r\n", value.len(), value))),
        None => Ok(Some(format!("$-1\r\n"))),
    }
}

fn handle_set(kv_store: &mut KvStore, command: Vec<String>) -> Result<Option<String>> {
    if command.len() != 3 {
        return error::InvalidRequest {
            message: "SET command format: SET <key> <value>".to_string(),
        }.fail();
    }

    kv_store.set(command[1].clone(), command[2].clone())?;

    Ok(None)
}

fn handle_rm(kv_store: &mut KvStore, command: Vec<String>) -> Result<Option<String>> {
    if command.len() != 2 {
        return error::InvalidRequest {
            message: "RM command format: RM <key>".to_string(),
        }.fail();
    }

    kv_store.remove(command[1].clone())?;

    Ok(None)
}

fn handle_request(kv_store: &mut KvStore, req: redis::types::RespTerm) -> Result<Option<String>> {
    let command_strs = try_req_to_string_array(req)?;

    match &command_strs[0][..] {
        "GET" => handle_get(kv_store, command_strs),
        "SET" => handle_set(kv_store, command_strs),
        "RM" => handle_rm(kv_store, command_strs),
        _ => error::InvalidRequest {
                message: format!("Invalid command: {}", command_strs[0]),
        }.fail(),
    }
}

fn handle_stream(kv_store: &mut KvStore, buffer: &mut Vec<u8>, stream: std::io::Result<TcpStream>) -> Result<()> {
    let mut stream = stream.context(error::ReadingStream)?;
    log::debug!(
        "CONNECTION -> peer addr: {}",
        stream.peer_addr().context(error::ReadingStream)?,
    );

    let mut buf_reader = io::BufReader::new(&stream);
    let request = redis::deserializer::RespDeserializer::new(&mut buf_reader)
        .read_term(buffer)?;

    let response = handle_request(kv_store, request)?;

    drop(buf_reader);
    if let Some(data) = response {
        stream.write(data.as_bytes()).context(error::WritingStream)?;
        stream.flush().context(error::WritingStream)?;
    }

    Ok(())
}

fn main() -> Result<()> {
    init_logger().expect("Unable to init logger");
    let version_number = dotenv!("CARGO_PKG_VERSION");
    let yaml = load_yaml!("cli.yml");
    let matches = App::from_yaml(yaml)
        .name(dotenv!("NAME"))
        .version(version_number)
        .author(dotenv!("AUTHOR"))
        .about(dotenv!("DESCRIPTION"))
        .get_matches();

    if matches.is_present("version") {
        println!("{}", version_number);
        std::process::exit(0);
    }

    let addr: String = matches.value_of("ip_address")
        .unwrap_or("127.0.0.1:4000")
        .to_string();
    let addr: SocketAddr = match addr.parse() {
        Ok(parsed) => parsed,
        Err(_) => return error::Generic {
            message: "Unable to parse socket addr".to_string(),
        }.fail(),
    };

    let engine: String = matches.value_of("engine")
        .unwrap_or("kvs")
        .to_string();

    let current_dir = env::current_dir().context(error::ManipulatingFile)?;
    let mut kv_store = KvStore::open(path::Path::new(current_dir.to_str().unwrap()))?;
    let listener = TcpListener::bind(addr).context(error::FailedServerConn)?;
    let mut buffer: Vec<u8> = vec![0; 1024];

    log::info!("KVS SERVER -> version: {}, address: {}, engine: {}", version_number, addr, engine);

    for stream in listener.incoming() {
        handle_stream(&mut kv_store, &mut buffer, stream)
            .unwrap_or_else(handle_error);
    }

    Ok(())
}
