use clap::{load_yaml, App};
use dotenv_codegen::dotenv;
use std::{self, env, path};
use snafu::ResultExt;

use kvs::{kv_store::KvStore, Result, error};

fn handle_error<T>(err: error::Error) -> T {
    println!("{}", err);

    std::process::exit(-1);
}

fn main() -> Result<()> {
    let yaml = load_yaml!("cli.yml");
    let matches = App::from_yaml(yaml)
        .name(dotenv!("NAME"))
        .version(dotenv!("CARGO_PKG_VERSION"))
        .author(dotenv!("AUTHOR"))
        .about(dotenv!("DESCRIPTION"))
        .get_matches();

    if matches.is_present("version") {
        println!("{}", dotenv!("CARGO_PKG_VERSION"));
        std::process::exit(0);
    }

    let current_dir = env::current_dir().context(error::ManipulatingFile)?;
    let mut kv_store = KvStore::open(path::Path::new(current_dir.to_str().unwrap()))?;

    match matches.subcommand() {
        ("get", Some(stub)) => {
            let result: Option<String> = kv_store
                .get(stub.value_of("key").unwrap().to_owned())
                .unwrap_or_else(handle_error);

            match result {
                Some(value) => println!("{}", value),
                None => println!("Key not found"),
            };
            std::process::exit(0);
        }
        ("set", Some(stub)) => {
            kv_store
                .set(
                    stub.value_of("key").unwrap().to_owned(),
                    stub.value_of("value").unwrap().to_owned(),
                )
                .unwrap_or_else(handle_error);

            std::process::exit(0);
        }
        ("rm", Some(stub)) => {
            kv_store
                .remove(stub.value_of("key").unwrap().to_owned())
                .unwrap_or_else(handle_error);

            std::process::exit(0);
        }
        _ => std::process::exit(1),
    };
}
