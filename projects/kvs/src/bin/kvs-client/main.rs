use clap::{self, load_yaml, App};
use dotenv_codegen::dotenv;
use snafu::ResultExt;
use std::{
    io::{self, prelude::*},
    net::{TcpStream, SocketAddr},
};

use kvs::{Result, error, redis};

fn handle_error<T>(err: error::Error) -> T {
    println!("{}", err);

    std::process::exit(-1);
}

fn get_addr(stub: &clap::ArgMatches) -> Result<SocketAddr> {
    let addr: String = stub.value_of("ip_address")
        .unwrap_or("127.0.0.1:4000")
        .to_string();
    match addr.parse::<SocketAddr>() {
        Ok(parsed) => Ok(parsed),
        Err(_) => error::Generic {
            message: "Unable to parse socket addr".to_string(),
        }.fail(),
    }
}

fn handle_get(stub: &clap::ArgMatches) -> Result<()> {
    let key: String = stub.value_of("key").unwrap().to_string();

    let addr = get_addr(stub)?;
    let mut stream = TcpStream::connect(addr).context(error::FailedServerConn)?;
    let mut buffer: Vec<u8> = vec![0; 20];

    let command_string = format!("*2\r\n$3\r\nGET\r\n${}\r\n{}\r\n", key.len(), key);

    stream.write(command_string.as_bytes()).context(error::WritingStream)?;
    stream.flush().context(error::WritingStream)?;
    let mut reader = io::BufReader::new(stream);
    let response = redis::deserializer::RespDeserializer::new(&mut reader)
        .read_term(&mut buffer)?;

    match response {
        redis::types::RespTerm::BulkStr(val) => println!("{}", val),
        redis::types::RespTerm::Null(_) => println!("Key not found"),
        _ => eprintln!("Invalid response type from the server"),
    };

    std::process::exit(0);
}

fn handle_set(stub: &clap::ArgMatches) -> Result<()> {
    let key: String = stub.value_of("key").unwrap().to_string();
    let value: String = stub.value_of("value").unwrap().to_string();

    let addr = get_addr(stub)?;
    let mut stream = TcpStream::connect(addr).context(error::FailedServerConn)?;

    let command_string = format!(
        "*3\r\n$3\r\nSET\r\n${}\r\n{}\r\n${}\r\n{}\r\n",
        key.len(),
        key,
        value.len(),
        value,
    );

    stream.write(command_string.as_bytes()).context(error::WritingStream)?;

    std::process::exit(0);
}

fn handle_rm(stub: &clap::ArgMatches) -> Result<()> {
    let key: String = stub.value_of("key").unwrap().to_string();

    let addr = get_addr(stub)?;
    let mut stream = TcpStream::connect(addr).context(error::FailedServerConn)?;

    let command_string = format!("*2\r\n$2\r\nRM\r\n${}\r\n{}\r\n", key.len(), key);

    stream.write(command_string.as_bytes()).context(error::WritingStream)?;

    std::process::exit(0);
}

fn main() {
    let yaml = load_yaml!("cli.yml");
    let matches = App::from_yaml(yaml)
        .name(dotenv!("NAME"))
        .version(dotenv!("CARGO_PKG_VERSION"))
        .author(dotenv!("AUTHOR"))
        .about(dotenv!("DESCRIPTION"))
        .get_matches();

    if matches.is_present("version") {
        println!("{}", dotenv!("CARGO_PKG_VERSION"));
        std::process::exit(0);
    }

    match matches.subcommand() {
        ("get", Some(stub)) => handle_get(&stub)
            .unwrap_or_else(handle_error),
        ("set", Some(stub)) => handle_set(&stub)
            .unwrap_or_else(handle_error),
        ("rm", Some(stub)) => handle_rm(&stub)
            .unwrap_or_else(handle_error),
        _ => std::process::exit(1),
    };
}
